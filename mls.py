#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

"""
Created on Mon Feb 15 19:06:39 2021

@author: Christophe Bovet (christophe.bovet@gmail.com)

A simple interface to PyMumps (need PyMumps and mpi4py).
mls stands for MumpsLocalSolver
    
> PyMumps is here: https://pypi.org/project/PyMUMPS/
> mpi4py is here: https://pypi.org/project/mpi4py/

In case of parallel computation, all data are localized on proc #0 and mumps
do the data distribution. 
So, only the proc #0 needs to setup: the operator, the rhs and get the solution

Exemple : 
> mpirun -np 1 python3 mls.py 
> mpirun -np 2 python3 mls.py 



"""

# Classic imports
import numpy as np
import scipy.linalg as la
import scipy.sparse as spa
import scipy.sparse.linalg as sla

# Less classic imports
import mumps


def laplace(n, format="coo"):
    """ 
    A really complexe 1D laplace operator with fixed endpoints
    Symmetric and full rank
    """    
    main_diag = 2. * np.ones(n)
    main_diag[0] = 1.
    main_diag[-1] = 1.
    diagonals = [ main_diag, -np.ones(n-1), -np.ones(n-1)]    
    out = spa.diags(diagonals, [0, -1, 1], shape = (n,n), format=format)
    return out

def laplace_full_rank(n, format="coo"):
    """ 
    A really complexe 1D laplace operator with fixed endpoints
    Symmetric and full rank
    """    
    diagonals = [ 2. * np.ones(n), -np.ones(n-1), -np.ones(n-1)]    
    return spa.diags(diagonals, [0, -1, 1], shape = (n,n), format=format)



class MumpsLocalSolver:
    """ """
    def __init__(self, spm, sym=0, par=1, rank_deficient=False, comm=None):
        """ 
        Important: 
            - spm is supposed to be a scipy sparse matrix 
            - and thus use C indices (start at 0)
        """
        # some useful variables
        self._is_factorized = False
        self._rank_deficient = rank_deficient
        self._shape = spm.shape
        self._nullity = None
        
        if(sym==1) and rank_deficient:
            print("Info: rank deficiency is expected, automatic switch to LDLT")
            sym=2
                
        # Conversion to coo format (if needed) and setup irn, jrn fortran like
        # Caution, if the symmetric flag is set you must provided only the 
        # upper triangular part of the matrix
        if sym:
            coo = spa.triu(spm.tocoo())
        else:
            coo = spm.tocoo()
        
        f_row = coo.row + 1 
        f_col = coo.col + 1 
        
        # First check
        self._basic_check(coo)
                
        # Create the MUMPS context and set the array and right hand side
        # sym = 1 (symmetric matrix)
        # par = 1 (the proc #0 does participate to the job)
        self._mumps = mumps.DMumpsContext(sym=sym, par=par, comm=comm)        
#        self.set_silent()
        # Really simple version (which does not make use of the parallelism of 
        # Mumps 
        if self._mumps.myid == 0:
            self._mumps.set_shape(coo.shape[0])            
            self._mumps.set_centralized_assembled(f_row, f_col, coo.data)
        
        if rank_deficient: 
            self._mumps.set_icntl(13, 1) # Disable Scalapack 
            self._mumps.set_icntl(24, 1) # Enable null pivots detection            

        return
    
    def __del__(self):
        """ To avoid memory leak inside mumps """
        self._mumps.destroy() 
        
    def factorize(self):
        """ Only do the symbolic analysis and numerical factorization """
        self._mumps.run(4)
        self._is_factorized = True
        
        if(self._rank_deficient):
            self._nullity = self._mumps.id.infog[27]
            print("Mumps found a kernel of size :", self._nullity)
    
    
    def compute_kernel(self):
        """ Allow the computation of the kernel """
        assert(self._rank_deficient and self._is_factorized)
        
        kernel = None
        x = np.zeros(self._shape[0], dtype='d')
        if self._mumps.myid == 0:            
            self._mumps.set_rhs(x)
            kernel = np.zeros((self._shape[0], self._nullity), dtype='d')
        
        # TODO: 
        # self._mumps.set_icntl(25, -1) allow to compute the full nullspace 
        # in one shot but it is not interface by PyMumps.
        # If needed I (CB) can improve PyMumps to allow that
        for i in range(self._nullity):
            self._mumps.set_icntl(25, i+1) # Ask for the ith kernel vector
            self._mumps.run(3)
            if self._mumps.myid == 0: kernel[:,i] = x
        
        return kernel 
    
    def solve(self, rhs):
        """ 
        Once factorized, do the forward and backward substitutions
        In mumps, the rhs is modified inplace so we do a copy
        """
        #TODO:  check on the dtype of rhs
        assert(self._is_factorized)

        # Ask for one of the possible solutions
        self._mumps.set_icntl(25, 0) 
        
        x = None
        if self._mumps.myid == 0: 
            x = rhs.copy()
            self._mumps.set_rhs(x)
        self._mumps.run(3)
        return x
        
    def _basic_check(self, A):
        """ Common check for clarity """
        n, m = A.shape
        if(n != m):
            raise ValueError("Only square matrix allowed")
        
        # TODO: check on the dtype of A

    def set_silent(self):
        """ """
        self._mumps.set_silent() # Turn off verbose output



def test1():
    """ 
    A really simple test from dsimpletest.py of PyMumps.
    The matrix is full rank but not symmetric    
    """
    # irn, jcn use Fortran indice (start at 1)
    n = 5
    irn = np.array([1,2,4,5,2,1,5,3,2,3,1,3], dtype='i')
    jcn = np.array([2,3,3,5,1,1,2,4,5,2,3,3], dtype='i')    
    a = np.array([3.0,-3.0,2.0,1.0,3.0,2.0,4.0,2.0,6.0,-1.0,4.0,1.0], dtype='d')
    rhs = np.array([20.0,24.0,9.0,6.0,13.0], dtype='d')
    A = spa.coo_matrix((a, (irn-1, jcn-1)), shape=(n,n))
    mls = MumpsLocalSolver(A, sym=0)
    mls.factorize()
    x = mls.solve(rhs)
        
    # Solution is known by #0
    if(mls._mumps.myid == 0):
        x_ex = np.array([1., 2., 3., 4., 5.], dtype='d')
        assert(np.allclose(x, x_ex))
    return
    
def test2(n=10):
    """
    A really complexe 1D laplace operator with fixed endpoints
    Symmetric and full rank
    But the symmetry **is not** exploited by mumps (LU)
    """
    A = laplace_full_rank(n)    
    rhs = np.ones(n)
    mls = MumpsLocalSolver(A, sym=0)
    mls.factorize()
    x_mls = mls.solve(rhs)

    # Solution is known by #0
    if(mls._mumps.myid == 0):
        x_spsolve = sla.spsolve(A.tocsc(), rhs)
        assert(np.allclose(x_mls, x_spsolve))
        
def test3(n=10):
    """
    A really complexe 1D laplace operator with fixed endpoints
    Symmetric and full rank
    The symmetry **is**  exploited by mumps (Crout)
    """
    A = laplace_full_rank(n)    
    rhs = np.ones(n)
    mls = MumpsLocalSolver(A, sym=1)
    mls.factorize()
    x_mls = mls.solve(rhs)
        
    # Solution is known by #0
    if(mls._mumps.myid == 0):
        x_spsolve = sla.spsolve(A.tocsc(), rhs)
        assert(np.allclose(x_mls, x_spsolve))


def test4(n=10):
    """
    A really complexe 1D laplace operator symmetric and with a 1D-kernel
    uniform values     
    """
    A = laplace(n)    
    
    # FIXME: there version without exploiting symmetry seems bugged 
    # mls = MumpsLocalSolver(A, sym=0, rank_deficient=True)
    
    mls = MumpsLocalSolver(A, sym=1, rank_deficient=True)
    mls.factorize()
    
    # Kernel computation
    kernel = mls.compute_kernel() 
    print(kernel)    
    rhs = None
    if(mls._mumps.myid == 0):
        # Force that rhs does not act on the kernel (solvability condition)
        assert(np.allclose(A.dot(kernel), 0.0))
        rhs = np.arange(n, dtype='d') + 1.0 
        rtk = np.dot(rhs.T, kernel)
        ktk = np.dot(kernel.T, kernel)    
        rhs[:] -= np.dot(kernel,  np.dot(la.inv(ktk), rtk))
        assert(np.allclose(np.dot(rhs.T, kernel),0.0))

    # Asking for one solution:
    x_mls = mls.solve(rhs)
    
    # Solution is known by #0
    if(mls._mumps.myid == 0):
        assert(np.allclose(rhs, A.dot(x_mls)))

    
    
    
if __name__ == "__main__":
    
    # Simple test without kernel (from dsimpletest.py)
    test1()
    
    # Test symmetric without kernel, mumps use LU
    test2()
    
    # Test symmetric without kernel, mumps use LLT
    test3()
    
    # Test symmetric **with** kernel, mumps use LDLT
    test4()